//
//  SetProfileVC.swift
//  Showcase
//
//  Created by Mehdi Gilanpour on 7/11/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

class SetProfileVC: UIViewController ,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    @IBOutlet weak var textField: MaterialTextFeild!
    @IBOutlet weak var image: UIImageView!
    var imagePicker : UIImagePickerController!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        textField.delegate = self
        image.layer.cornerRadius = image.frame.size.height / 2
        image.clipsToBounds = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        self.image.image = image
    }
    
    @IBAction func addPic(sender: UIButton) {
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    
    //Keyboard Handling
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }

    @IBAction func finish(sender: MaterialButton) {
        if let img = image.image where img != UIImage(named: "profile-empty"){
            let urlStr = "https://post.imageshack.us/upload_api.php"
            let url = NSURL(string: urlStr)!
            
            let imgData = UIImageJPEGRepresentation(img, 0.2)!
            let key = IMAGESHACK_KEY.dataUsingEncoding(NSUTF8StringEncoding)
            let JSONKey = "json".dataUsingEncoding(NSUTF8StringEncoding)
            
            Alamofire.upload(.POST, url, multipartFormData: { multipartFormData in
                multipartFormData.appendBodyPart(data:imgData,name:"fileupload",fileName:"image" , mimeType:"image/jpg")
                multipartFormData.appendBodyPart(data:key!,name:"key")
                multipartFormData.appendBodyPart(data:JSONKey!,name:"format")
                
                
                }) { encodingResults in
                    
                    switch encodingResults {
                        
                    case .Success(let upload, _ , _):
                        upload.responseJSON{ response in
                            if let res = response.result.value as? Dictionary<String,AnyObject>{
                                if let links = res["links"] as? Dictionary<String ,AnyObject>{
                                    if let imageLink = links["image_link"] as? String{
                                        print("LINKS: \(imageLink)")
                                        Manager.manager.REF_USER_CURRENT.child("ProfilePic").setValue(imageLink)
                                        MainVC.imageCache.setObject(self.image.image!, forKey: imageLink)
                                        Manager.manager.imageURL = imageLink

                                    }
                                    
                                }
                            }
                        }
                    case .Failure(let error):
                        print(error)
                    }
                    
                    
            }
            
            
        }
     
        downloadData{()->() in
        
            if Manager.manager.flag{
                Manager.manager.REF_USER_CURRENT.child("Username").setValue(self.textField.text)
                Manager.manager.username = self.textField.text!
                self.performSegueWithIdentifier(SEGUES_MAIN_FROM_PROFILE, sender: nil)
                
            }else{
                self.showError("This username already exists", msg: "Try somthing else")
                self.textField.text = ""
                Manager.manager.flag = true
            }
        
        }
    
    }
    func downloadData(completed:downloadCompleted){
        
        if let txt = self.textField.text where txt != ""{
            Manager.manager.REF_USERS.observeSingleEventOfType(.Value, withBlock: {snapshot in
                
                if let users = snapshot.children.allObjects as? [FIRDataSnapshot]{
                    
                    for user in users {
                        let userNameRef = user.childSnapshotForPath("Username")
                        if let username = userNameRef.value as? String{
                            if username == txt{
                                Manager.manager.flag = false
                                print("FALSE")
                            }
                        }
                    }
                    
                }
                completed()
                
            })
           
            
            
        }
        
    }
    
    func showError (title:String,msg:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        let Action = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(Action)
        alert.view.tintColor = UIColor.purpleColor()
        presentViewController(alert, animated: true, completion: nil)
    }

}
