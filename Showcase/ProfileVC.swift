//
//  ProfileVC.swift
//  Showcase
//
//  Created by Mehdi Gilanpour on 7/12/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

class ProfileVC: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    var imagePicker : UIImagePickerController!
    @IBOutlet weak var email: MaterialLabel!
    @IBOutlet weak var username: MaterialLabel!
    @IBOutlet weak var image: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker = UIImagePickerController()
        imagePicker.delegate = self

        Manager.manager.REF_USER_CURRENT.child("ProfilePic").observeEventType(.Value, withBlock: {snapshot in
        
            Manager.manager.imageURL = (snapshot.value as? String)!

            if Manager.manager.imageURL != "" {
                if MainVC.imageCache.objectForKey(Manager.manager.imageURL) as? UIImage == nil{
                    Alamofire.request(.GET, Manager.manager.imageURL).validate(contentType: ["image/*"]).response(completionHandler:{request,response,data,error in
                    
                        if error == nil{
                            self.image.image = UIImage(data: data!)
                            MainVC.imageCache.setObject(UIImage(data: data!)!, forKey: Manager.manager.imageURL)
                            
                        }
                    
                    })
                }else{
                    self.image.image = MainVC.imageCache.objectForKey(Manager.manager.imageURL) as? UIImage
                }

            }else{
                self.image.image = UIImage(named: "profile-empty")
            }
        
        })

        image.layer.cornerRadius = image.frame.size.height / 2
        image.clipsToBounds = true
        
        email.text = Manager.manager.email
        username.text = Manager.manager.username
        


    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        self.image.image = image
        
        
        change("Changing your profile picture !", msg: "Are you sure you want this picture as your profile picture?")
        

        
    }


    @IBAction func changePis(sender: UIButton) {
        
        	changPicDialoge("Go to my gallery", msg: "Are you sure? ")
        
    }
    
    func changPicDialoge (title:String,msg:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        let Action = UIAlertAction(title: "YES", style: .Default, handler: {action in
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        })
        let Action2 = UIAlertAction(title: "NO", style: .Default, handler: nil)
        alert.addAction(Action)
        alert.addAction(Action2)
        presentViewController(alert, animated: true, completion: nil)
    }
    func change (title:String,msg:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        let Action = UIAlertAction(title: "YES", style: .Default, handler: {action in
            let urlStr = "https://post.imageshack.us/upload_api.php"
            let url = NSURL(string: urlStr)!
            
            let imgData = UIImageJPEGRepresentation(self.image.image!, 0.2)!
            let key = IMAGESHACK_KEY.dataUsingEncoding(NSUTF8StringEncoding)
            let JSONKey = "json".dataUsingEncoding(NSUTF8StringEncoding)
            
            Alamofire.upload(.POST, url, multipartFormData: { multipartFormData in
                multipartFormData.appendBodyPart(data:imgData,name:"fileupload",fileName:"image" , mimeType:"image/jpg")
                multipartFormData.appendBodyPart(data:key!,name:"key")
                multipartFormData.appendBodyPart(data:JSONKey!,name:"format")
                
                
                }) { encodingResults in
                    
                    switch encodingResults {
                        
                    case .Success(let upload, _ , _):
                        upload.responseJSON{ response in
                            if let res = response.result.value as? Dictionary<String,AnyObject>{
                                if let links = res["links"] as? Dictionary<String ,AnyObject>{
                                    if let imageLink = links["image_link"] as? String{
                                        print("LINKS: \(imageLink)")
                                        Manager.manager.REF_USER_CURRENT.child("ProfilePic").setValue(imageLink)
                                        MainVC.imageCache.setObject(self.image, forKey: imageLink)
                                        Manager.manager.imageURL = imageLink
                                        
                                    }
                                    
                                }
                            }
                        }
                    case .Failure(let error):
                        print(error)
                    }
                    
                    
            }
        })
        let Action2 = UIAlertAction(title: "NO", style: .Default, handler: {action in
        
            Manager.manager.REF_USER_CURRENT.child("ProfilePic").observeEventType(.Value, withBlock: {snapshot in
                
                Manager.manager.imageURL = (snapshot.value as? String)!
                print(Manager.manager.imageURL)
                if Manager.manager.imageURL != ""{
                    
                    self.image.image = MainVC.imageCache.objectForKey(Manager.manager.imageURL) as? UIImage
                }
                
                
            })
        })
        alert.addAction(Action)
        alert.addAction(Action2)
        alert.view.tintColor = UIColor.purpleColor()
        presentViewController(alert, animated: true, completion: nil)
    }
}
