//
//  Constants.swift
//  Showcase
//
//  Created by Mehdi Gilanpour on 7/2/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import Foundation
import UIKit

let SHADOW_COLOR: CGFloat = 157.0 / (255.0)
//Keys
let KEY_UID = "uid"

//Segues
let SEGUES_LOGGED_IN = "loggedin"
let SEGUES_MAIN = "main"
let SEGUES_MAIN_FROM_PROFILE = "mainFromProfile"
let SEGUES_GO_TO_FIRST = "goToFirst"

//Status code
let STATUS_USER_NOT_FOUND = 17011
let STATUS_CAN_NOT_CONNECT = 17999
let STATUS_EMAIL_WRONG = 17008
let STATUS_WEAK_PASSWORD = 17026
let STATUS_USERS_EXISTS = 17007
let STATUS_WRONG_PASSWORD = 17009


//ImageShackKEY
let IMAGESHACK_KEY = "NBR1PTZAb0196880c497d5a9479c7dabaed2736f"

typealias downloadCompleted = () -> ()


let postNumberConstant = 5

