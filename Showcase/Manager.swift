//
//  Manager.swift
//  Showcase
//
//  Created by Mehdi Gilanpour on 7/2/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import Foundation
import Firebase

let BASE_URL = "https://showcase-465c2.firebaseio.com"

class Manager{

    static let manager = Manager()
    
    public var flag = true
    public var firstTime = true
    public var load = false
    public var clickCount = postNumberConstant

    
    public var username:String = ""
    public var email:String = ""
    public var imageURL:String = ""

    public var rootRef = FIRDatabase.database().reference()
    
    public var REF_BASE = FIRDatabase.database().referenceFromURL(BASE_URL)
    public var REF_USERS = FIRDatabase.database().referenceFromURL("\(BASE_URL)/Users")
    public var REF_POSTS = FIRDatabase.database().referenceFromURL("\(BASE_URL)/Posts")
    
    var REF_USER_CURRENT : FIRDatabaseReference{
        let User = FIRAuth.auth()?.currentUser
        let uid = User?.uid
        let user = REF_USERS.child(uid!)
        return user
    }
    
    func creatUser (uid:String , user: Dictionary < String , String>){
        
        REF_USERS.child(uid).setValue(user)
    
    }

}