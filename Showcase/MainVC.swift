//
//  MainVC.swift
//  Showcase
//
//  Created by Mehdi Gilanpour on 7/3/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

class MainVC: UIViewController , UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate{
    @IBOutlet weak var BarButton: UIBarButtonItem!
    @IBOutlet weak var activePost: UIActivityIndicatorView!
    @IBOutlet weak var postCaption: MaterialTextFeild!
    @IBOutlet weak var cameraPicture: UIImageView!
    @IBOutlet weak var active: UIActivityIndicatorView!
    @IBOutlet weak var tableview: UITableView!
    var posts = [Post]()
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var activeMorePost: UIActivityIndicatorView!
    var imagePicker : UIImagePickerController!
    static var imageCache = NSCache()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activeMorePost.hidden = true
        
        tableview.delegate = self
        tableview.dataSource = self
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        tableview.estimatedRowHeight = 390
        
        let usernameRef = Manager.manager.REF_USER_CURRENT
        usernameRef.child("Username").observeEventType(.Value, withBlock: {snapshot in
        
            self.BarButton.title = snapshot.value as? String
            Manager.manager.username = (snapshot.value as? String)!
        })
        
        let query = Manager.manager.REF_POSTS.queryOrderedByChild("Time")
        query.queryLimitedToLast(UInt(postNumberConstant)).observeEventType(.Value, withBlock: { snapshot in
            
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                self.posts = []
                for snap in snapshots {
                    if let postDict = snap.value as? Dictionary<String, AnyObject> {
                        print(postDict)
                        let post = Post(postKey: snap.key,  dic: postDict)
                        self.posts.append(post)
                    }
                }
                
                self.posts = self.posts.reverse()
                self.tableview.reloadData()
            }
            
        })
        
        /*Manager.manager.REF_POSTS.observeEventType(.Value, withBlock:{ snapshot in
        
            self.posts = []
            
            if let snaps = snapshot.children.allObjects as? [FIRDataSnapshot]{
                
                for snap in snaps {
                    if let postDic = snap.value as? Dictionary<String,AnyObject> {
                        let key = snap.key
                        let newPost = Post(postKey: key, dic: postDic)
                        self.posts.append(newPost)
                        
                        if !Manager.manager.firstTime{
                            if let userID = postDic["User"] as? String{
                                Manager.manager.REF_USERS.child(userID).child("Username").observeEventType(.Value, withBlock:{ snapshot in
                                    if let username = snapshot.value as? String{
                                        let notification:UILocalNotification = UILocalNotification()
                                        if username != Manager.manager.username{
                                            notification.alertBody = "New post from \(username)"
                                            notification.alertAction = "Action"
                                            notification.fireDate = NSDate(timeIntervalSinceNow: 1)
                                            print("FIRE")
                                            UIApplication.sharedApplication().scheduleLocalNotification(notification)
                                        }
                                        
                                    }
                                    
                                })
                            }
                        
                        }
     
                    }
                }
                
            }
            Manager.manager.firstTime = false
            self.tableview.reloadData()
            
        })*/
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        if indexPath.row < posts.count {
            let post = posts[indexPath.row]

            if let cell = tableView.dequeueReusableCellWithIdentifier("PostCell") as? PostCell{
                
                cell.request?.cancel()
                var img: UIImage?
                
                if let url = post.imageURL{
                    img = MainVC.imageCache.objectForKey(url) as? UIImage
                }
                
                cell.configureCell(post,img: img)
                self.active.hidden=true
                Manager.manager.load = true
                return cell
            }
            else{
                return PostCell()
            }
        }else{
            if let cell = tableview.dequeueReusableCellWithIdentifier("loadMoreCell") as? loadMoreCell{
                if Manager.manager.load{
                    cell.loadMoreOutlet.hidden = false
                }
                return cell
            }else{
                return loadMoreCell()
            }
            
        }
        
        }
        
        
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row < posts.count{
            let post = posts[indexPath.row]
            
            if post.imageURL == nil{
                return 200
            }else{
                return tableview.estimatedRowHeight
            }
        }else{
            return 60
        }
       
        
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        cameraPicture.image = image
    }
  		
    @IBAction func cameraTapped(sender: UITapGestureRecognizer) {
        presentViewController(imagePicker, animated: true, completion: nil)
        
    }
    @IBAction func PostIt(sender: MaterialButton) {
        if let txt = self.postCaption.text where txt != ""{
            if let img = cameraPicture.image where img != UIImage(named: "camera"){
                self.activePost.hidden = false
                let urlStr = "https://post.imageshack.us/upload_api.php"
                let url = NSURL(string: urlStr)!
                //converting our stuffs into data to pass to Imageshack servers 
                //for more info see upload part of this address : https://code.google.com/archive/p/imageshackapi/wikis/ImageshackAPI.wiki
                
                let imgData = UIImageJPEGRepresentation(img, 0.2)!
                let key = IMAGESHACK_KEY.dataUsingEncoding(NSUTF8StringEncoding)
                let JSONKey = "json".dataUsingEncoding(NSUTF8StringEncoding)
                
                Alamofire.upload(.POST, url, multipartFormData: { multipartFormData in
                multipartFormData.appendBodyPart(data:imgData,name:"fileupload",fileName:"image" , mimeType:"image/jpg")
                    multipartFormData.appendBodyPart(data:key!,name:"key")
                    multipartFormData.appendBodyPart(data:JSONKey!,name:"format")

                    
                    }) { encodingResults in
                        
                        switch encodingResults {
                        
                        case .Success(let upload, _ , _):
                            upload.responseJSON{ response in
                                if let res = response.result.value as? Dictionary<String,AnyObject>{
                                    if let links = res["links"] as? Dictionary<String ,AnyObject>{
                                        if let imageLink = links["image_link"] as? String{
                                            print("LINKS: \(imageLink)")
                                            self.uploadToFirebase(imageLink)
                                            self.tableview.reloadData()
                                        }
                                        
                                    }
                                }
                            }
                        case .Failure(let error):
                            print(error)
                        }
                        
                        
                }
            }else{
                self.uploadToFirebase(nil)
            }
        }
        
    }
    func uploadToFirebase(imageURL:String?){
       let firebasePost = Manager.manager.REF_POSTS.childByAutoId()
        
        let str = firebasePost.URL
        let newStr = (str as NSString).stringByReplacingOccurrencesOfString("\(Manager.manager.REF_POSTS.URL)/", withString: "")
        print(newStr)
        
        var post : Dictionary<String,AnyObject> = [
        
            "Caption": self.postCaption.text! , "Likes": 0 
        
        ]
        
        if imageURL != nil{
            post["imageURL"] = imageURL!
        }
        post["User"] = FIRAuth.auth()?.currentUser?.uid
        post["Time"] = stringTime()
        firebasePost.setValue(post)
        
        
        Manager.manager.REF_USER_CURRENT.child("Posts").child(newStr).setValue(true)
        
        
        self.postCaption.text = ""
        self.cameraPicture.image = UIImage(named: "camera")
        self.activePost.hidden = true
        self.tableview.reloadData()
    }
    @IBAction func logout(sender: UIBarButtonItem) {
        

        
            try! FIRAuth.auth()!.signOut()
            Manager.manager.username = ""
            Manager.manager.flag = true
            Manager.manager.imageURL = ""
            Manager.manager.email = ""
            
            self.performSegueWithIdentifier(SEGUES_GO_TO_FIRST, sender: nil)

        

        
        
    }
    
    @IBAction func cancel(sender: MaterialButton) {
        
        self.postCaption.text = ""
        self.cameraPicture.image = UIImage(named: "camera")
        
    }
    
    @IBAction func option(sender: MaterialButton) {
        
        let alert = UIAlertController(title: "Choose option", message: "You can change your post here.", preferredStyle: .Alert) // 1
        
        let firstAction = UIAlertAction(title: "Edit", style: .Default) { (alert: UIAlertAction!) -> Void in
        } // 2
        
        let secondAction = UIAlertAction(title: "Delete", style: .Default) { (alert: UIAlertAction!) -> Void in
           
        } // 3
        let thiredAction = UIAlertAction(title: "Cancel", style: .Default) { (alert: UIAlertAction!) -> Void in

        } // 3
        
        alert.addAction(firstAction) // 4
        alert.addAction(secondAction) // 5
        alert.addAction(thiredAction)
        alert.view.tintColor = UIColor.purpleColor()

        presentViewController(alert, animated: true, completion:nil) // 6

        
    }
    
    func stringTime () -> String{
        let date = NSDate()
        // : "May 10, 2016, 8:55 PM" - Local Date Time
        var formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
        let defaultTimeZoneStr = formatter.stringFromDate(date)
        // : "2016-05-10 20:55:06 +0300" - Local (GMT +3)
        formatter.timeZone = NSTimeZone(abbreviation: "UTC")
        let utcTimeZoneStr = formatter.stringFromDate(date)
        // : "2016-05-10 17:55:06 +0000" - UTC Time
        
        return utcTimeZoneStr
    }
    
    
    @IBAction func load(sender: MaterialButton) {
        self.activeMorePost.hidden = false
        self.activeMorePost.startAnimating()
        self.mainView.alpha = 0.2
        self.activeMorePost.alpha = 1.0
        let query = Manager.manager.REF_POSTS.queryOrderedByChild("Time")
        query.queryLimitedToLast(UInt(Manager.manager.clickCount + postNumberConstant)).observeEventType(.Value, withBlock: { snapshot in
            
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                self.posts = []
                for snap in snapshots {
                    if let postDict = snap.value as? Dictionary<String, AnyObject> {
                        
                        let post = Post(postKey: snap.key,  dic: postDict)
                        self.posts.append(post)
                    }
                }
                
                self.posts = self.posts.reverse()
                self.activeMorePost.hidden = true
                self.mainView.alpha = 1.0
                Manager.manager.clickCount += postNumberConstant
                self.tableview.reloadData()
            }
            
        })
    }
    
}







