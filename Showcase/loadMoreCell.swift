//
//  loadMoreCellTableViewCell.swift
//  Showcase
//
//  Created by Mehdi Gilanpour on 7/16/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit

class loadMoreCell: UITableViewCell {

    @IBOutlet weak var loadMoreOutlet: MaterialButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.loadMoreOutlet.hidden = true
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    


  
}
