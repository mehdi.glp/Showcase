//
//  PostCell.swift
//  Showcase
//
//  Created by Mehdi Gilanpour on 7/3/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class PostCell: UITableViewCell {

    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var optionButton: MaterialButton!
    @IBOutlet weak var likesPic: UIImageView!
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var caption: UITextView!
    @IBOutlet weak var postPic: UIImageView!
    
    var likeRef:FIRDatabaseReference!
    
    var post:Post!
    var request : Request?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let tap = UITapGestureRecognizer(target: self, action: "likeTapped:")
        tap.numberOfTouchesRequired = 1
        likesPic.addGestureRecognizer(tap)
        likesPic.userInteractionEnabled = true
        
    }
    
    override func drawRect(rect: CGRect) {
        profilePic.layer.cornerRadius = profilePic.frame.size.height / 2
        profilePic.clipsToBounds = true
        
        postPic.layer.cornerRadius = 4.0
        postPic.clipsToBounds = true
    }

    func configureCell(post: Post , img:UIImage?){
        self.likeRef = Manager.manager.REF_USER_CURRENT.child("Likes").child(post.postKey)
        
        post.postREF.child("User").observeEventType(.Value, withBlock: {snapshot in
            let uid = snapshot.value as? String
            Manager.manager.REF_USERS.child(uid!).observeEventType(.Value, withBlock: {snapshot in
            
                if let userDic = snapshot.value as? Dictionary<String,AnyObject>{
                
                    if let username = userDic["Username"] as? String{
                        self.profileLabel.text = username
                        self.optionButton.hidden = false
                        if self.profileLabel.text != Manager.manager.username{
                            self.optionButton.hidden = true
                        }
                        
                    }
                    
                   
                    
                    let profilePic = userDic["ProfilePic"] as? String
                        if profilePic != ""{
                        
                            if MainVC.imageCache.objectForKey(profilePic!) as? UIImage == nil {
                                Alamofire.request(.GET, profilePic!).validate(contentType: ["image/*"]).response(completionHandler:{request,response,data,error in
                                    
                                    if error == nil {
                                        let img = UIImage(data: data!)
                                        self.profilePic.image = img
                                        MainVC.imageCache.setObject(img!, forKey: profilePic!)
                                    }
                                    
                                })
                            }else{
                                self.profilePic.image = MainVC.imageCache.objectForKey(profilePic!) as? UIImage
                            }
                        
                        
                        }else{
                            self.profilePic.image = UIImage(named: "profile-empty")
                    }
                
                }
            
            })
        
        })
   

        self.post = post
        self.caption.text = post.caption
        self.likes.text = "\(post.likes)"
        self.date.text = self.post.time
        
        if post.imageURL != nil{
            if img != nil {
                self.postPic.image = img
            }else{
                request = Alamofire.request(.GET, post.imageURL!).validate(contentType: ["image/*"]).response(completionHandler:{request,response,data,error in
                    if error == nil{
                        let img = UIImage(data: data!)!
                        self.postPic.image = img
                        MainVC.imageCache.setObject(img, forKey: self.post.imageURL!)
                    }
                    else{
                        print(error.debugDescription)
                    }
                })
            }
            
        }else{
            self.postPic.hidden = true
        }
        
        self.likeRef = Manager.manager.REF_USER_CURRENT.child("Likes").child(post.postKey)
        likeRef.observeSingleEventOfType(.Value, withBlock:{snapshot in
        
        
            if  (snapshot.value as? NSNull != nil){
                self.likesPic.image = UIImage(named: "heart-empty")
            }else{
                self.likesPic.image = UIImage(named: "heart-full")
            }
        
        
        } )
        
    }
    
    
    func likeTapped(sender : UITapGestureRecognizer){
        self.likeRef = Manager.manager.REF_USER_CURRENT.child("Likes").child(post.postKey)
        likeRef.observeSingleEventOfType(.Value, withBlock:{snapshot in
            
            
            if  (snapshot.value as? NSNull != nil){
                self.likesPic.image = UIImage(named: "heart-full")
                self.post.adjustLike(true)
                self.likeRef.setValue(true)
            }else{
                self.likesPic.image = UIImage(named: "heart-empty")
                self.post.adjustLike(false)
                self.likeRef.removeValue()

            }

        } )
    }
}







