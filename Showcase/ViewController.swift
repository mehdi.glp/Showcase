//
//  ViewController.swift
//  Showcase
//
//  Created by Mehdi Gilanpour on 7/2/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var active: UIActivityIndicatorView!
    @IBOutlet weak var password: MaterialTextFeild!
    @IBOutlet weak var email: MaterialTextFeild!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
        email.delegate = self
        password.delegate = self
        self.navigationController?.navigationBar.hidden = true
        

        
    }
    
    //Keyboard Handling
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }

    @IBAction func ButtonPressed(sender: UIButton) {
        Manager.manager.email = email.text!
        active.hidden = false
       if let Email = email.text where Email != "" , let pass = password.text where pass != ""{
                FIRAuth.auth()?.signInWithEmail(Email, password: pass) { (user, error) in
                    print("Error 1 : \(error)")
                if error != nil{
                    if error!.code == STATUS_EMAIL_WRONG{
                        self.showError("Problem in login", msg: "The email address is badly formatted.")
                    }
                    if error!.code == STATUS_WRONG_PASSWORD{
                        self.showError("Problem in login", msg: "The password is invalid or the user does not have a password.")
                    }
                    FIRAuth.auth()!.createUserWithEmail(Email, password: pass, completion: { authData, error  in
                        print("Error 2 : \(error)")
                        if error?.code == STATUS_WEAK_PASSWORD{
                            self.showError("Problem in Signup", msg: "The password must be 6 characters long or more.")
                        }
                        if error?.code == STATUS_WEAK_PASSWORD{
                            self.showError("Problem in Signup", msg: "The email address is already in use by another account.")
                        }
                        if error == nil {
                            // Log user in
                            FIRAuth.auth()?.signInWithEmail(Email, password: pass) { authData, error in
                                // Save user information to Firebase data
                                let User = ["Email":Email , "Username":"" , "ProfilePic":""] // your user Dictionary
                                print("USER:   \(User.values)")
                                Manager.manager.creatUser(authData!.uid, user: User)
                                self.performSegueWithIdentifier(SEGUES_MAIN, sender: nil)
                            }
                        } else {
                            // Handle login error here
                            self.showError("Error !", msg: "Somthing is wrong. Try again")
                            self.active.hidden = true
                        }
                    })
                    
                }else{
                    let usernameRef = Manager.manager.REF_USER_CURRENT
                    usernameRef.child("Username").observeEventType(.Value, withBlock: {snapshot in
                        if let username = snapshot.value as? String{
                            if username == ""{
                                print("IN 1")
                                self.performSegueWithIdentifier(SEGUES_MAIN, sender: nil)
                                
                            }else{
                                print("IN 2")
                                self.performSegueWithIdentifier(SEGUES_LOGGED_IN, sender: nil)
                                
                            }
                        }
                    })

                    
                }
        }
        
       }else{
        self.showError("Error !", msg: "Username or Password could not be empty.")
        self.active.hidden = true
        }
        
        /*self.active.hidden = false
        if let Email = email.text where Email != "" , let pass = password.text where pass != "" {
            
            Manager.manager.BASE_REF.authUser(Email, password: pass, withCompletionBlock:{error , authData in
            
                if error != nil{
                    
                    if error.code == STATUS_EMAIL_IS_WRONG{
                        
                        self.active.hidden = true
                        self.showError("Wrong Email", msg: "Please enter a valid email")
                    
                    }
                    if error.code == STATUS_PASSWORD_IS_WRONG{
                        
                        self.active.hidden = true
                        self.showError("Could not login", msg: "Username or Password is wrong")
                        
                    }
                    if error.code == STATUS_USER_NOT_EXIST{
                    
                        Manager.manager.REF_BASE.createUser(Email, password: pass, withValueCompletionBlock: { error , result in
                            
                            if error != nil{
                            
                                self.showError("Could not creat account", msg: "Try something else")
                            
                            }else{
                                
                                NSUserDefaults.standardUserDefaults().setValue(result[KEY_UID], forKey: KEY_UID)
                                
                                Manager.manager.REF_BASE.authUser(Email, password: pass, withCompletionBlock: { error , authData in
                                    
                                    let User = [ "Username":Email]
                                    //Manager.manager.creatUser(authData.uid, user: User)
                                })
                                
                                Manager.manager.REF_BASE.authUser(Email, password: pass, withCompletionBlock: nil)
                                self.performSegueWithIdentifier(SEGUES_LOGGED_IN, sender: nil)
                                self.active.hidden = true
                                
                                
                                
                                
                            }
                            
                            
                        })
                    
                    }
                
                    print(error)
                
                }else{
                
                    self.performSegueWithIdentifier(SEGUES_LOGGED_IN, sender: nil)
                    self.active.hidden = true
                
                }
            
            } )
            
        }
        else{
            self.active.hidden = true
            showError("Email and Password required", msg: "You must enter email and a password")
            
        }*/
        
    }
    
    
    func showError (title:String,msg:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .Alert)
        let Action = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(Action)
        alert.view.tintColor = UIColor.purpleColor()
        presentViewController(alert, animated: true, completion: nil)
    }

}

