//
//  MaterialTextFeild.swift
//  Showcase
//
//  Created by Mehdi Gilanpour on 7/2/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import UIKit

class MaterialTextFeild: UITextField {

    override func awakeFromNib() {
        layer.cornerRadius = 2.0
        layer.shadowColor = UIColor(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).CGColor
        layer.borderWidth = 0.0
    }
    
    //For placeholder
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, 10, 0)
    }
    
    //For editable text
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, 10, 0)
    }

}
