//
//  Post.swift
//  Showcase
//
//  Created by Mehdi Gilanpour on 7/8/16.
//  Copyright © 2016 Mehdi Gilanpour. All rights reserved.
//

import Foundation
import Firebase

class Post{
    
    public var caption:String!
    public var imageURL:String?
    public var likes:Int!
    public var username:String!
    public var postKey:String!
    public var postREF:FIRDatabaseReference!
    public var user:String!
    public var time:String!
    
    init(caption:String,imageURL:String?,username:String){
        self.caption = caption
        self.imageURL = imageURL
        self.username = username
    }
    
    init(postKey:String,dic:Dictionary<String,AnyObject>){
        self.postKey = postKey
        
        if let likes = dic["Likes"] as? Int{
            print(likes)
            self.likes = likes
        }
        
        if let cap = dic["Caption"] as? String{
            print(cap)
            self.caption = cap
        }
        
        if let image = dic["imageURL"] as? String{
            print(image)
            self.imageURL = image   
        }
        if let user = dic["User"] as? String{
            print(user)
            self.user = user
        }
        if let time = dic["Time"] as? String{
            print(time)
            let str = time
            let newStr = (str as NSString).stringByReplacingOccurrencesOfString("+0000", withString: "")
            self.time = newStr
        }
        
        self.postREF = Manager.manager.REF_POSTS.childByAppendingPath(self.postKey)
    }
    
    func adjustLike (addLike:Bool){
        if addLike {
            likes = likes + 1
        }else{
            likes = likes - 1
        }
        
        postREF.childByAppendingPath("Likes").setValue(likes)
    }
    
}